# Lecture 9 Assignments - The cartpole problem
## Creating the environment
If you want to create a new environment, here are the packages that work:

`conda create -n drl tensorflow`
`conda activate drl`
`pip install gym`
`pip install stable-baselines`
`conda install -c conda-forge tensorboard`

In case you encounter that `tensorflow` is missing libraries, you can try to downgrade to version 1.13:
`conda uninstall tensorflow`
`conda install -c conda-forge tensorflow=1.13`

## Training the agent
Training parameters, such as the model and the training time (in steps) can be adjusted in the `cartploe_example.py` file
Once you ran trained the agent, you can set the `train_model` variable to `False`, which will render the trained agent.

If you wish to access the tensorboard information:
`tensorboard --logdir YourLogFolderName/`
### Training the agent for 65000 steps (DQN)
[Image 21](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/cartpole.gif)

And the corresponding Tensorboard log:

[Image 22](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/L9_TrainingPoleTensorboard.PNG)

As you can see, the performance is stable, but the movement is periodic. A significant improvement can be achieved by training the agent longer.
### Training the agent for 100000 steps (DQN)
[Image 23](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/cartpole_long.gif)

And the corresponding Tensorboard log:

[Image 24](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/L9_TrainingPoleTensorboard_long.PNG)

The agent is significantly more stable, although it took it a couple of seconds to stabilize (which is why it´s shifted to the left)
### Comparing DQN to Asynchronous Actor-Critic (A2C)
We compared two methods, to see how the performance compares.
Here is the corresponding Tensorboard log:

[Image 25](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/L9_TrainingPoleTB_compare.PNG)
