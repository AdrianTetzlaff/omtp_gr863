Lecture 1 Assignments
=======

Building a Visual Robot Model with URDF from Scratch
-----------

1) We followed the tutorial on "Building a Visual Robot Model with URDF from Scratch" found on [Tutorial 1](http://wiki.ros.org/urdf/Tutorials/Building%20a%20Visual%20Robot%20Model%20with%20URDF%20from%20Scratch)

It all worked without any problems.




Building a Movable Robot Model with URDF
-----------

2) We followed the tutorial on "Building a Movable Robot Model with URDF" found on [Tutorial 2](http://wiki.ros.org/urdf/Tutorials/Building%20a%20Movable%20Robot%20Model%20with%20URDF)

It all worked without any problems.




Using Xacro to Clean Up a URDF File
-----------

3) We followed the tutorial on "Using Xacro to Clean Up a URDF File" found on [Tutorial 3](http://wiki.ros.org/urdf/Tutorials/Using%20Xacro%20to%20Clean%20Up%20a%20URDF%20File)

It all worked without any problems.




Cleaning up our OMTP factory world
-----------

4) We transformed the .xacro file into a .urdf file and checked the syntax.

   * Remove Bin 2-5

We removed the following lines from the xacro file to remove bins 2-5  
[Image 1](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/Selection_001.png)    

[Image 2](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/Selection_002.png)  

   * Move bin 1 to station 2

We changed the position of bin 1 to  
[Image3](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/Selection_003.png)  

   * Add UR5 Robot

5) We added another UR5 robot using the existing XACRO file.

 We added the  following lines to the xacro file to add the robot   
[Image 4](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/Selection_004.png)

[Image 5](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/Selection_005.png)

Add ABB IRB6400 Robot

We added an ABB IRB6400 robot by adding the following lines  
[Image6](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/Selection_007.png)   
 
[Image 7](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/Selection_010.png)  

We downloaded the ABB urdf and xacro files from [Link to ABB](https://github.com/ros-industrial/abb)

   * Add simple geometry

We added different geometrical shapes by adding the following lines  
[Image8](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/Selection_008.png) 
   
[Image9](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/Selection_009.png)  

The final scene looks like this

[Image 10](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/Selection_006.png)  
