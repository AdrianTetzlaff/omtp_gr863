
import gym 
from stable_baselines.common.vec_env import DummyVecEnv 
#from stable_baselines.deepq.policies import MlpPolicy
from stable_baselines.common.policies import MlpPolicy
from stable_baselines import DQN, A2C
# CartPole-v1
env = gym.make('CartPole-v1')
#model = DQN(MlpPolicy, env, verbose=1, tensorboard_log='./tensorBoardlogs')
model = A2C(MlpPolicy, env, verbose=1, tensorboard_log='./tb_A2C')

 

# Set whether to train or test
train_model = False
model_filename = 'dqn_cartpole_100000'
learnSteps = 100000


# Train model
if train_model:
    model.learn(total_timesteps=learnSteps)
    model.save(model_filename)
# Test model
else:
    # del model # remove to demonstrate saving and loading
    model = A2C.load(model_filename)

    # Enjoy trained agent
    obs = env.reset()
    while True:
        action, _states = model.predict(obs)
        obs, rewards, dones, info = env.step(action)
        env.render(mode='human')
 