Lecture 5 Assignments
=======

Manipulation with MoveIt
-----------



Since we were working on a Full Desktop Install of ROS Melodic we did not need to install any dependencies.



Create a MoveIt package using MoveIt Setup Assistant
-----------

1) We created the MoveIt package for the factory environment with help of the MoveIt Setup Assistant.

The package can be found [here](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/src/omtp_moveit_config/)



Use MoveIt Commander to execute a script of motions
------------

2) We wrote a script to move the robot in a given sequence as requested in the slides

The script can be found [here](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/src/omtp_moveit_config/launch/moveit_commander_test)



Complete lecture5_assignment3.py
------------------

3) We implemented the python script, which is [here](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/src/omtp_lecture5/scripts/lecture5_assignment3.py)

[Image 11](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/lec5.gif)
