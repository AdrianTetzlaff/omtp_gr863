# Lecture 8 Assignments - COVID-19 Mask detection script 
This Lecture we trained a CNN model to recognise if people are having respiratory masks on.

[Image 17](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/L8_Mask_example3.png)

## Extended functionality

Now it´s possible to specify video files instead of the camera feed with the `-v filename.extension` parameter (works for any known video extensions)

It´s also possible to record video files with the `-r filename` parameter, the recording is in .avi

Please note that `CV2` only allows recording that matches the input video or camera feed resolution.

## ROB863 running the script:


[Image 18](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/L8_markGif.gif)

[Image 19](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/adrianMaskRotated.gif)

[Image 20](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/karolinaMask.gif)
## Setting up the environment
This tutorial assumes the use of [Anaconda](https://www.anaconda.com/products/individual).
There are two possibilities to set up the environment:
### Import the attached Anaconda environment file
This solution out-of-the-box, but you will not get familiar with the packages used
Simply [create an environment](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file) with the yml file by running:

`conda env create -f tf.yml`


### Create a virtual environment from scratch
This solution guides you through all the necessary installs
`conda install -c conda-forge tensorflow`
`conda install -c conda-forge imutils`
(Make sure you don´t have spyder open)
PIL to open images
`conda install -c anaconda pillow`

## Running the script
You can run the script from the working directory by the following commands:

### Training the model
`python train_mask_detector.py -d dataset`

### Detect mask in an image
`python detect_mask_image.py -i yourimage.png`

### Detect mask in a video
`python detect_mask_video.py`
