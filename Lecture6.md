Lecture 6 Assignments
=======

Object Detection and grasping
-----------

Add two cameras
-----------

1) We included two cameras [here](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/src/omtp_gazebo/worlds/lecture6_assignment1.world)

2) We configured the two cameras [here](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/src/omtp_gazebo/models/logical_camera1/model.sdf) and [here](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/src/omtp_gazebo/models/logical_camera2/model.sdf)

And then we successfully ran `$ roslaunch omtp_lecture6 lecture6_assignment1.launch`

TF View_frames
-----------

3) We executed `$ roslaunch omtp_lecture6 lecture6_assignment1.launch`

4) We ran `$ rosrun tf view_frames`

5) We got this image

[Image 12](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/frames.jpeg)


Transform Object Pose
-----------

6) We start `$ roslaunch omtp_lecture6 lecture6_assignment3.launch`

7) We call `$ rosservice call /spawn_object_at_robot2`

8) We completed the script [lecture6_assignment3.py](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/src/omtp_lecture6/scripts/lecture6_assignment3.py)

9) We ran `$ rosrun omtp_lecture6 lecture6_assignment3.py`



Pick up objects
-----------

We experienced certain difficulties, which we could not solve

[Image 13](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/RobotFail.gif)

[Image 14](https://bitbucket.org/AdrianTetzlaff/omtp_gr863/src/master/img/RobotFail2.gif)
