#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from flexbe_states.wait_state import WaitState
from omtp_factory_flexbe_states.compute_grasp_state import ComputeGraspState
from omtp_factory_flexbe_states.moveit_to_joints_dyn_state import MoveitToJointsDynState
from omtp_factory_flexbe_states.vacuum_gripper_control_state import VacuumGripperControlState
from omtp_factory_flexbe_states.detect_part_camera_state import DetectPartCameraState
from omtp_factory_flexbe_states.set_conveyor_power_state import SetConveyorPowerState
from omtp_factory_flexbe_states.control_feeder_state import ControlFeederState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Wed Jun 10 2020
@author: Adrian Herskind
'''
class Pickpartfromconveyorwithrobot1SM(Behavior):
	'''
	This behavior will make robor1 pick a part from the conveyor in our factory
	'''


	def __init__(self):
		super(Pickpartfromconveyorwithrobot1SM, self).__init__()
		self.name = 'Pick part from conveyor with robot1'

		# parameters of this behavior

		# references to used behaviors

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		pick_group = 'robot1'
		home1 = [-1.57, -1.57, -1.57, -1.57, 1.57, 0]
		robot1_joint_names = ['robot1_elbow_joint', 'robot1_shoulder_lift_joint', 'robot1_shoulder_pan_joint', 'robot1_wrist_1_joint', 'robot1_wrist_2_joint', 'robot1_wrist_3_joint']
		robot1_tool_link = 'vacuum_gripper1_suction_cup'
		speed = 50
		stopSpeed = 0
		PreGrasp = [1.57, -1.44, -0.01, -1.61, -1.54, 1.30]
		jointNames = ['robot1_elbow_joint', 'robot1_shoulder_lift_joint', 'robot1_shoulder_pan_joint', 'robot1_wrist_1_joint', 'robot1_wrist_2_joint', 'robot1_wrist_3_joint']
		# x:1213 y:678, x:130 y:530
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		_state_machine.userdata.part_pose = []
		_state_machine.userdata.pick_configuration = home1
		_state_machine.userdata.joint_names = []
		_state_machine.userdata.home1 = home1
		_state_machine.userdata.speed = speed
		_state_machine.userdata.stopSpeed = stopSpeed
		_state_machine.userdata.PreGrasp = PreGrasp
		_state_machine.userdata.jointNames = jointNames

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]


		with _state_machine:
			# x:0 y:154
			OperatableStateMachine.add('wait10',
										WaitState(wait_time=5),
										transitions={'done': 'Move Conveyor'},
										autonomy={'done': Autonomy.Off})

			# x:718 y:50
			OperatableStateMachine.add('Compute Grasp Configuration',
										ComputeGraspState(group='robot1', offset=0, joint_names=robot1_joint_names, tool_link=robot1_tool_link, rotation=3.1415),
										transitions={'continue': 'Move Robot1 To Pick', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'part_pose', 'joint_values': 'pick_configuration', 'joint_names': 'joint_names'})

			# x:904 y:105
			OperatableStateMachine.add('Move Robot1 To Pick',
										MoveitToJointsDynState(move_group=pick_group, offset=0, tool_link=robot1_tool_link, action_topic='/move_group'),
										transitions={'reached': 'Activate Gripper', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'pick_configuration', 'joint_names': 'joint_names'})

			# x:1066 y:179
			OperatableStateMachine.add('Activate Gripper',
										VacuumGripperControlState(enable='true', service_name='/gripper1/control'),
										transitions={'continue': 'Move robot1 to home configuration', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off})

			# x:1177 y:253
			OperatableStateMachine.add('Move robot1 to home configuration',
										MoveitToJointsDynState(move_group=pick_group, offset=0, tool_link=robot1_tool_link, action_topic='/move_group'),
										transitions={'reached': 'finished', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'home1', 'joint_names': 'joint_names'})

			# x:354 y:34
			OperatableStateMachine.add('Detect Part',
										DetectPartCameraState(ref_frame='robot1_base_link', camera_topic='/omtp/logical_camera', camera_frame='logical_camera_frame'),
										transitions={'continue': 'Stop Conveyor', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'part_pose'})

			# x:513 y:34
			OperatableStateMachine.add('Stop Conveyor',
										SetConveyorPowerState(stop=True),
										transitions={'succeeded': 'Compute Grasp Configuration', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'speed': 'stopSpeed'})

			# x:48 y:47
			OperatableStateMachine.add('Move Conveyor',
										SetConveyorPowerState(stop=False),
										transitions={'succeeded': 'Conveyor', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'speed': 'speed'})

			# x:207 y:102
			OperatableStateMachine.add('Conveyor',
										ControlFeederState(activation=True),
										transitions={'succeeded': 'Detect Part', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
